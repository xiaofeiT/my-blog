---
title: Git 向多个仓库提交代码
date: 2020-01-13 18:26:48
tags:
	- 工具
	- Git
---



本地仓库配置多个源，并分别向这几个源推送代码。

<!-- more -->

#### 查看当前远端仓库

```bash
git remote -v

# 可以看到当前的远程仓库，正常情况下有两个，一个是 fetch，一个是 push
```

#### 添加一个远端仓库

这里以 Github 和 Coding.net 为例

`git remote add both git@github.com:username/project.git`

终端中是没有任何提示的，但是远端仓库是已经建立好的。

将 `username` 替换成自己的账号名字，`project`改为自己的项目名称

#### 为新建的远端仓库添加两个要push的地址

```bash
git remote set-url --add --push git@github.com:username/project/git
git remote set-url --add --push git@git.coding.net:username/project.git
```

> 注意，这样添加的ssh方式，需要提前配置好 SSH 公钥

#### 推送到两个仓库

使用一下命令就可以同时推送到两个仓库了：

```bash
git push both
```

#### 删除新建的库

我们可以使用下列命令来删除新建的库：

```bash
git remote remove remoteName
```

将 `remoteName` 改为要删除的库名

可以使用：

`git remote -v`

来查看当前目录下的所有库名。